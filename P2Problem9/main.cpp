#include <iostream>
#include<string.h>
#include<stdlib.h>
using namespace std;
// Ruben Florez Carroll cc. 1193522164 Grupo 1.

/*
 Programa que recibe un n�mero entero ( int) n y lee una cadena de caracteres num�ricos,
 el programa debe separar la cadena de caracteres en n�meros de n cifras, sumarlos e imprimir el resultado.
En caso de no poderse dividir exactamente en n�meros de n cifras se colocan ceros a la izquierda del primer n�mero.
 Ejemplo: Si n=3 y se lee el arreglo 87512395 la suma seria 087+512+395=994.
Nota: la salida del programa debe ser: Original: 87512395. Suma: 994.
 */
int main()
{ int numero,cont=1,suma=0;
    char CadenaCaracterNumerico[20];
    int  nuevacadena;
    cout << "Ingrese una cadena de caracteres numerica:"<<endl;
    cin.getline(CadenaCaracterNumerico,20);
    cout << "Ingrese un numero:" << endl;
    cin>>numero;
    nuevacadena=atoi(CadenaCaracterNumerico); //Funcion atoi transforma cadena de caracteres en entero
   cout<<"Nuevacadena:"<<nuevacadena<<endl;

    for (int i=0; i<numero;i++)
    {
    cont*=10; // El valor del contador dependera del numero ingresado
    }                // Esto nos ayudara a separar la cadena de numeros

    int cadena;

    for (int i=0; i<= (strlen(CadenaCaracterNumerico))/numero;i++)
    {

     cadena=nuevacadena%cont; // se iran tomando los numeros de n cifras

     nuevacadena=nuevacadena/cont; // modificacion de la cadena para poder tomar el siguiente numero de n cifras
      suma=suma+cadena; // Suma de los numeros de n cifras de la cadena separada
    }
cout<<"Original: "<<CadenaCaracterNumerico<<endl;
cout<<"Suma: "<<suma<<endl;

    return 0;
}


