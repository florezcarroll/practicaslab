 #include <iostream>
using namespace  std;
// Ruben Florez Carroll cc. 1193522164. Grupo 1
/*
Dos n�meros a y b (a != b) son amigables si la suma de los divisores de a (excluy�ndose el mismo) es igual a b,
y viceversa. Ej: los divisores de 220 son 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 y 110; y suman 284. Los divisores de 284 son 1, 2, 4, 71 y 142;
y suman 220.Entonces 220 y 284 son amigables.
Este  programa recibe un n�mero entero y
halla la suma de todos los n�meros amigables menores que el
Ejemplo si se ingrese el numero entero 284, el siguiente numero amigable es el 220, por lo tanto... el resultado de la suma es: 504.

 */

int main()
{ int numero;
   int divisores;
cout<<"Ingrese numero: "<<endl;
cin>>numero;
int numerooriginal=numero; // variable para almacenar el valor original del numero ingresado por el usuario

while (numero>1)
{ int suma=0;

    for (int i=1;i<numero;i++) //Hallar divisores de cada numero
    {
               divisores=numero%i;

              if (divisores == 0)
                 {
                     suma=suma+i; // suma de los divisores encontrados
                  }

      }
              if(suma==numerooriginal) // Son amigos, el resultado de la suma de los divisores de un numero es igual al numeor original
              {
               cout<<numerooriginal<<" y "<<numero<<" son amigos "<<endl;

               cout<<"La suma de los numeros  amigos es: "<<numero+numerooriginal<<endl;  // suma del numero encontrado y el numero original

              }     numero--; // decrecimiento del numero, para asi verificar cada numero menor que el original



     }

return 0;
}
