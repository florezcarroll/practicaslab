#include <iostream>

using namespace std;
// Ruben Florez Carroll - CC. 1193522164 - Grupo 1

/* Funcionamiento del programa: Este programa reparte una suma de dinero en cada denominacion de billetes y/o monedas
 Cuando reparta las cantidades de cada billete y moneda, si no logra completar la suma total de dinero, al final debera
decir cuanto es el dinero restante, el cual no se pudo repartir.


Las varibles de este programa seran de tipo entero y se usaran variables para cada denominacion de billete y moneda
ademas se utilizaran variables para saber la cantidad  de cada una y variables para  el nuevo total de dinero que queda
faltando por repartir...

Si se ingresa un monto de dinero X este dara al final como resultado la cantidad de billetes de  $50.000, $20.000,
$10.000, $5.000, $2.000 y $1.000 y  monedas  de $500, $200, $100 y $50. Ademas del dinero que quede faltando, el cual no
se logre repartir entre cada denominacion.
*/
int main()
{ // Denominaciones de cada billete y moneda
  int billete50k=50000, billete20k=20000, billete10k=10000,billete5k=5000,billete2k=2000,billete1k=1000;
  int moneda500=500,moneda200=200, moneda100=100, moneda50=50;

  int DineroIngresado;

  int CantidadBilletes50k=0, RestaDeDiv50k=0,CantidadBilletes20k=0, RestaDeDiv20k=0;
  int CantidadBilletes10k=0, RestaDeDiv10k=0,CantidadBilletes5k=0,RestaDeDiv5k=0;
  int CantidadBilletes2k=0,RestaDeDiv2k=0, CantidadBilletes1k=0,RestaDeDiv1k=0;
  int CantidadMonedas500=0,RestaMonedas500=0,CantidadMonedas200=0,RestaMonedas200=0;
  int CantidadMonedas100=0, RestaMonedas100=0, CantidadMonedas50=0, RestaMonedas50=0;

  int RestaFinal=0;
  cout<<"Ingrese la cantidad de dinero:"<<endl;
  cin>>DineroIngresado;

   if(DineroIngresado>=moneda50) // Este condicional sirve para  indicar el funcionamiento del programa, la condicion es si el dinero ingresado es mayor o igual a la menor cantidad establecida de denominacion de moneda.
                                 // Si se ingresa un numero menor a la menor moneda, en este caso $50, el programa solo imprimiria el dinero restante menor que el valor de la menor moneda.
      {
       CantidadBilletes50k=DineroIngresado/billete50k;
       RestaDeDiv50k=DineroIngresado-(billete50k*CantidadBilletes50k);
cout<<"Billetes de $50.000 : "<<CantidadBilletes50k<<endl;



       CantidadBilletes20k=RestaDeDiv50k/billete20k;
       RestaDeDiv20k=RestaDeDiv50k-(billete20k*CantidadBilletes20k);

cout<<"Billetes de $20.000 : "<<CantidadBilletes20k<<endl;


       CantidadBilletes10k=RestaDeDiv20k/billete10k;
       RestaDeDiv10k=RestaDeDiv20k-(billete10k*CantidadBilletes10k);
cout<<"Billetes de $10.000 : "<<CantidadBilletes10k<<endl;


       CantidadBilletes5k=RestaDeDiv10k/billete5k;
       RestaDeDiv5k=RestaDeDiv10k-(billete5k*CantidadBilletes5k);
cout<<"Billetes de $5.000 : "<<CantidadBilletes5k<<endl;

       CantidadBilletes2k=RestaDeDiv5k/billete2k;
       RestaDeDiv2k=RestaDeDiv5k-(billete2k*CantidadBilletes2k);
cout<<"Billetes de $2.000 : "<<CantidadBilletes2k<<endl;


       CantidadBilletes1k=RestaDeDiv2k/billete1k;
       RestaDeDiv1k=RestaDeDiv2k-(billete1k*CantidadBilletes1k);
cout<<"Billetes de $1.000 : "<<CantidadBilletes1k<<endl;

       CantidadMonedas500=RestaDeDiv1k/moneda500;
       RestaMonedas500=RestaDeDiv1k-(moneda500*CantidadMonedas500);
cout<<"Monedas de $500 : "<<CantidadMonedas500<<endl;


       CantidadMonedas200=RestaMonedas500/moneda200;
       RestaMonedas200=RestaMonedas500-(moneda200*CantidadMonedas200);
cout<<"Monedas de $200 : "<<CantidadMonedas200<<endl;

       CantidadMonedas100=RestaMonedas200/moneda100;
       RestaMonedas100=RestaMonedas200-(moneda100*CantidadMonedas100);
cout<<"Monedas de $100 : "<<CantidadMonedas100<<endl;

       CantidadMonedas50=RestaMonedas100/moneda50;
       RestaMonedas50=RestaMonedas100-(moneda50*CantidadMonedas50);
cout<<"Monedas de $50 : "<<CantidadMonedas50<<endl;


   }
   // En esta variable se almacenara el dinero que falte por repartir, se le restara al dinero ingresado la suma de las  cantidades de cada billete o moneda.
RestaFinal=DineroIngresado - ( (CantidadBilletes50k*billete50k)+(CantidadBilletes20k*billete20k)+ (CantidadBilletes10k*billete10k) + (CantidadBilletes5k*billete5k) + (CantidadBilletes2k*billete2k) +(CantidadBilletes1k*billete1k) +(CantidadMonedas500*moneda500) +(CantidadMonedas200*moneda200) + (CantidadMonedas100*moneda100) +(CantidadMonedas50*moneda50));
cout<<"Faltante: "<<RestaFinal<<endl;
    return 0;
}
