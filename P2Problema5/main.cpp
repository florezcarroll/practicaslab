#include <iostream>
#include <string>
#include <sstream>
using namespace std;

//Ruben Florez Carroll cc 1193522164 Grupo 1
/*
 *funci�n que recibe un numero entero (int) y lo convierte a cadena de caracteres.
   Ejemplo: si recibe un int con valor 123, la cadena que se retorne debe ser �123�.
 */
string conversion(int& ); // Prototipo de la funcion
int main()
{   int numero;
    cout << "Ingrese un numero: ";cin>>numero;
    cout<<"numero;:"<<conversion(numero);
    return 0;
}
string conversion(int &num ) // implementacion de la funcion
{
    string Result;//Variable string que contendra el resultado

    stringstream convert; // stringstream usado para la conversion

    convert << num;//a�ade el valor del numero a los caracteres en el stream

    Result = convert.str();

    return Result;

}


