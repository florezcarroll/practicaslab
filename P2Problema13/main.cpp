#include <iostream>

using namespace std;
// Ruben Florez Carroll cc. 1193522164 Grupo 1.
/*
 * Se tiene una fotograf�a digitalizada de una porci�n de la galaxia NGC 1300 que est� ubicada a 61.000.000 de a�os luz del planeta Tierra. La representaci�n digital de la imagen est� constituida por una matriz de n�meros enteros; en la cual, cada uno representa la cantidad de luz en ese punto de la imagen, as�:
    0 3 4 0 0 0 6 8
    5 13 6 0 0 0 2 3
    2 6 2 7 3 0 10 0
    0 0 4 15 4 1 6 0
    0 0 7 12 6 9 10 4
    5 0 6 10 6 4 8 0
Se puede determinar si el elemento ai,j de la matriz representa una estrella si se cumple que: ai,j + ai,j-1 + ai,j+1 + ai-1,j + ai+1,j / (5)  > 6

El programa arroja como resultado el n�mero de estrellas encontradas en la imagen.
 */
int EstrellasEncontradas (int*);
int main()
{

  int filas, columnas;
 int elemento;

 int contestrellas=0;
int  matrizestrellas[100][100];
int  *puntero[100][100];

    cout << "Numero de filas:" << endl;
    cin>>filas;
     cout << "Numero de columnas:" << endl;
     cin>>columnas;

 puntero[filas][columnas]=&matrizestrellas[filas][columnas];


 for (int i=0;i<filas;i++)
       {
         for (int j=0;j<columnas;j++)

             {
            cout<<"Escriba un numero ["<<i<<"]["<<j<<"] ";
            cin>>matrizestrellas[i][j];

             }

        }

 //Matriz

    for (int i=0;i<filas;i++)
    {
        for (int j=0;j<columnas;j++)

        {
            cout<<matrizestrellas[i][j]<<"\t";

        }
        cout<<endl;
    }

 cout<<endl;



  // ESTR
cout<<"Matriz sin tener en cuenta bordes"<<endl;
   for (int i=1;i<filas-1;i++)
   {
       for (int j=1; j<columnas-1;j++)
       {


           cout<<matrizestrellas[i][j]<<"\t";
        } cout<<endl;

   }
// Elementos
   for (int i=1;i<filas-1;i++)
   {
       for (int j=1; j<columnas-1;j++)
       {


           elemento=matrizestrellas[i][j]+matrizestrellas[i][j-1]+matrizestrellas[i][j+1]+matrizestrellas[i-1][j]+matrizestrellas[i+1][j]; // Suma de las posiciones para hallar el elemento

           if ((elemento/5) >6)
           {
               contestrellas++;
           }

        } cout<<endl;

   }
cout<<"el numero de estrellas encontradas es:"<<contestrellas<<endl;

/*

    // ignorar bordes...
    for ( int i = 0; i <filas; i++ ) {
      for( int j = 0; j <columnas; j++ ) {
  if( i==0 || i==filas-1 || j==0 || j==columnas-1 )
      {

      }

      else{cout<<matrizestrellas[i][j]<<"\t";


       }

   }
//cout<<"elemento:"<<elemento;

cout<<endl;

  }



*/

    return 0;
}

/*

int EstrellasEncontradas (int*)

{



}
*/
