﻿#include <iostream>

using namespace std;
// Ruben Florez Carroll
/*
Escribaunprogramaquepermitamanejarlasreservasdeasientosenunasaladecine,
losasientosde la sala de cine están organizados en 15 ﬁlas con 20 asientos cada una.
El programa debe mostrar una representación de la sala que indique que asientos están disponibles y
cuales se encuentran reservados. Además debe permitir realizar reservas o cancelaciones al ingresar la ﬁla (letras A-O)
y el número del asiento (números 1-20). Nota: un ejemplo de visualización de una sección de la sala es el siguiente:
+ + + +
 - - +  +
 -  -  - -
Donde + representa los asientos reservados y - representa los asientos disponibles.
 */
int main()
{ int asientos[15][20]= {};
    int c=1, fila, columna;

    while (c<=300)
   {
        cout<<"Ingrese numero de fila (1-15)"<<endl;
        cin>>fila;
        cout<<"Ingrese numero de  columna (1-20)"<<endl;
        cin>>columna;


        if (asientos[fila-1][columna-1]==0)
        {
            asientos[fila-1][columna-1]=1;
            c+=1;
            cout<<"Reservacion realizada exitosamente!"<<endl; }
        else {

            cout<<"EL ASIENTO SE ENCUENTRA OCUPADO"<<endl;
            string cancelreserva;
        cout<<"Desea cancelar la reserva de este asiento? si/no : ";

        // Cancelacion de la reserva
        cin>>cancelreserva;
        if (cancelreserva=="si")
        {
             asientos[fila-1][columna-1]=0;
             cout<<"La reserva del asiento ha sido cancelada..."<<endl;
        }
        }

        cout<<"Asientos disposibles (+)"<<endl;

              for (int i=0; i <15;i++)
              {
                  for (int j=0;j<20;j++)
                  {
                 // Representacion de asientos ocupados ( - ) y disponibles ( + )
                     if(asientos[i][j]==0)
                     {
                         cout<<"+"<<"\t";

                     }
                     else {
                         cout<<"-"<<"\t";

                     }

                  }
              } cout<<endl;

    }

    return 0;
}
