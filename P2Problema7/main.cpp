#include <iostream>
#include <string>
using namespace std;
// Ruben Florez Carroll cc. 1193522164  Grupo 1
/*
  Programa que recibe una cadena de caracteres y elimina los caracteres repetidos.
  Ejemplo: si recibe bananas debe mostrar bs.
 Original: bananas. Sin repetidos: bs.
 */
// Prototipo de las funciones
bool SeRepite(char, string ); // Funcion para contar los caracteres que se repitan en la cadena.
void EliminarCoincidencias(char, string&); // Funcion para eliminar los caracteres que se repitan

 int main ()
 {
     string cadena;

     cout<<"Ingrese la cadena: "<<endl;
     getline(cin,cadena);
     int longitud=int(cadena.size());
  string cadenaoriginal=cadena;
     for ( int i=0; i<=longitud;i++)
     {
      if (SeRepite(cadena[i],cadena))
      {
         EliminarCoincidencias(cadena[i],cadena);
       }
}
     // Salida del programa :
cout<<"Original: "<<cadenaoriginal<<endl;
cout<<"Sin repetidos: "<<cadena<<endl;
     return 0;
 }




 // Implementacion de las funciones

 bool SeRepite( char b , string cad)
 {
     int longitud=int (cad.size()); // Longitud de la cadena de caracteres
     int cont=0; // contador que ayudara a contar cada vez que se repita un caracter

  for (int i=0; i<longitud;i++)
      {
            if (b == cad[i]) // verifica cada caracter de la cadena
            {
                cont++;
            }

      }

  if (cont>1) // si el contador es mayor a 1 es porque el caracter se repite
   {
  return true;
  }

else {
return false;
  }
 }

 void EliminarCoincidencias(char e , string &cad)
 {
  int longitud=int(cad.size());
     for ( int i=0; i <longitud; i++)
{
         if (e==cad[i])
     {
             cad.erase(i,1); // la funcion erase ayuda a eliminar un caracter de una cadena
              i--;
       }

     }

 }
